import anybadge
import re
import sys


thresholds = {100: 'red',
              75: 'orange',
              50: 'yellow',
              25: 'yellowgreen',
              10: 'green'}


def generateBadges(log_filename, output_path):
    table_start = False
    with open(log_filename) as f:
        usage_lines = [line.strip() for line in f]
    for i, line in enumerate(usage_lines):
        if "Utilization by Hierarchy" in line:
            table_start = True
        if table_start:
            if "top" in line and "top" not in usage_lines[i-1]:
                header = [j.strip() for j in usage_lines[i-2].split("|")][4:-1]
                values = [j.strip() for j in line.split("|")][4:-1]
                values = [re.search('\(([^)]+)', j).group(1)[:-1] for j in values]

    for i in range(len(header)):
        badge = anybadge.Badge(header[i], float(values[i]), thresholds=thresholds, value_suffix="%")
        badge.write_badge("{}/{}.svg".format(output_path, header[i]), overwrite=True)


if (__name__ == "__main__"):
    if len(sys.argv) > 1:
        usage_path = sys.argv[1]
        output_path = sys.argv[2]
    else:
        usage_path = "ci/usage.txt"
        output_path = "ci/badges"
    generateBadges(usage_path, output_path)

